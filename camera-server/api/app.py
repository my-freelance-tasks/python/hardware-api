#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify
from flask import make_response
from flask_cors import CORS, cross_origin
import gphoto2cffi as gp
import sys
import os
import random
import string

# from werkzeug.urls import Href

from flask import request
from pathlib import Path
import time


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


# print(app.root_path)

@app.route('/api/v0.1/capture', methods=['GET'])
@cross_origin()
def get_capture():
    cam_list = gp.list_cameras()
    cams_total = len(cam_list)
    captures = []
    os.system('/usr/bin/gphoto2 --reset')

    if cams_total == 0:
        return make_response(jsonify({'error': '45 No cameras connected'
                             }))
    else:
        cam = 0
        for cam in range(cams_total):
            camera_object = gp.list_cameras()[cam]

            try:
                imgdata = camera_object.capture()
            except Exception:
                cam_data = {'id': cam + 1,
                            'name': camera_object.model_name,
                            'error': str(sys.exc_info()[1])}
                captures.append(cam_data)
            else:
                currenttime = int(time.time())
                img_filename = '{}{}.JPG'.format(currenttime,
                        randomword(2))
                img_file_path = \
                    '{}/static/captured/{}/'.format(app.root_path, cam
                        + 1)
                if not os.path.exists(img_file_path):
                    os.makedirs(img_file_path)
                img_file_path = '{}{}'.format(img_file_path,
                        img_filename)
                img_file = open(img_file_path, 'wb+')
                img_file.write(imgdata)
                img_file.close()
                hostname = request.headers.get('Host')
                url_for_image = \
                    'http://{}/static/captured/{}/{}'.format(hostname,
                        cam + 1, img_filename)
                cam_data = {'id': cam + 1,
                            'name': camera_object.model_name,
                            'photo': url_for_image}
                captures.append(cam_data)
        return jsonify({'captures': captures})


@app.route('/api/v0.1/files', methods=['GET'])
@cross_origin()
def get_files():
    cam_list = gp.list_cameras()
    cams_total = len(cam_list)
    files_json = []
    os.system('/usr/bin/gphoto2 --reset')

    if cams_total == 0:
        return make_response(jsonify({'error': '45 No cameras connected'
                             }))
    else:
        cam = 0
        for cam in range(cams_total):
            camera_object = gp.list_cameras()[cam]

            try:

                # Get a list of files on the camera

                files = tuple(camera_object.list_all_files())
            except Exception:
                cam_data = {'id': cam + 1,
                            'name': camera_object.model_name,
                            'error': str(sys.exc_info()[1])}
                files_json.append(cam_data)
            else:
                if not files:
                    cam_data = {'id': cam + 1,
                                'name': camera_object.model_name,
                                'error': '400. Files Not Found'}
                    files_json.append(cam_data)
                else:
                    count_files = -1
                    for path in files:
                        count_files = count_files + 1

                        # cam_data = {'id' : cam+1, 'file' : path.name }
                        # files_json.append(cam_data)
                        # print(type(path.get_data()))

                        img_file_path = \
                            '{}/static/{}/'.format(app.root_path, cam
                                + 1)
                        if not os.path.exists(img_file_path):
                            os.makedirs(img_file_path)
                        img_filename = path.name
                        img_file_path = '{}{}'.format(img_file_path,
                                img_filename)
                        img_file = open(img_file_path, 'wb+')

                        try:
                            img_file.write(path.get_data())
                            img_file.close()
                        except Exception:

                            cam_data = {'id': cam + 1,
                                    'name': camera_object.model_name,
                                    'error': str(sys.exc_info()[1])}
                            files_json.append(cam_data)
                        else:
                            path.remove()

        directory = '{}/static/'.format(app.root_path)

        pathlist = Path(directory).glob('**/*.JPG')
        hostname = request.headers.get('Host')

        for path in pathlist:

            # because path is object not string

            path_in_str = str(path)

            # print(path_in_str)

            path_in_str = path_in_str.replace(app.root_path, '')
            url_for_image = 'http://{}{}'.format(hostname, path_in_str)
            if 'captured' in url_for_image:
                (beforek, keyw, afterk) = \
                    url_for_image.partition('/captured/')
                cam_numb = afterk.rpartition('/')[0]
                cam = int(cam_numb)
            else:
                (beforek, keyw, afterk) = \
                    url_for_image.partition('/static/')
                cam_numb = afterk.rpartition('/')[0]
                cam = int(cam_numb)

            cam_data = {'id': cam, 'file': url_for_image}
            files_json.append(cam_data)

        return jsonify({'files': files_json})


@app.route('/api/v0.1/delete', methods=['GET'])
@cross_origin()
def get_delete():
    delete_json = []
    path = request.args.get('path')
    img_file_path = '{}/static/{}'.format(app.root_path, path)
    if os.path.isfile(img_file_path):
        os.remove(img_file_path)
        file_data = {'code': '200', 'status': 'File removed'}
        delete_json.append(file_data)
    else:
        file_data = {'code': '404', 'status': 'File not found'}
        delete_json.append(file_data)

    return jsonify({'delete': delete_json})


@app.route('/api/v0.1/delete_all', methods=['GET'])
@cross_origin()
def get_delete_all():
    delete_json = []
    path = request.args.get('path')
    img_file_path = '{}/static/{}'.format(app.root_path, path)

    directory = '{}/static/'.format(app.root_path)

    pathlist = Path(directory).glob('**/*.JPG')

    try:

        for path in pathlist:

            # because path is object not string

            path_in_str = str(path)
            if os.path.isfile(path_in_str):
                os.remove(path_in_str)
    except Exception:
        file_data = {'code': '404', 'error': str(sys.exc_info()[1])}
        delete_json.append(file_data)
    else:
        file_data = {'code': '200', 'status': 'Files removed'}
        delete_json.append(file_data)

    return jsonify({'delete': delete_json})


@app.route('/api/v0.1/set_config', methods=['GET'])
@cross_origin()
def get_set_config():
    cam_list = gp.list_cameras()
    cams_total = len(cam_list)
    set_config = []
    os.system('/usr/bin/gphoto2 --reset')
    cam_number = request.args.get('cam')
    section = request.args.get('section')
    param = request.args.get('param')
    value = request.args.get('value')
    if cam_number is None:
        return make_response(jsonify({'error': '41 Argument in request is not specified. Argument cam'
                             }))
    if section is None:
        return make_response(jsonify({'error': '41 Argument in request is not specified. Argument section'
                             }))
    if param is None:
        return make_response(jsonify({'error': '41 Argument in request is not specified. Argument param'
                             }))
    if value is None:
        return make_response(jsonify({'error': '41 Argument in request is not specified. Argument value'
                             }))

    if cams_total == 0:
        return make_response(jsonify({'error': '45 No cameras connected'
                             }))
    else:
        try:
            cam = int(cam_number) - 1
            camera_object = gp.list_cameras()[cam]
        except Exception:
            return make_response(jsonify({'error': '46 Camera not found'
                                 }))
        else:
            try:
                camera_object.config[section][param].set(value)
                print(dir(camera_object.config[section][param]))
            except Exception:
                cam_data = {'id': cam + 1,
                            'error': str(sys.exc_info()[1])}
                set_config.append(cam_data)
            else:
                cam_data = \
                    {'status': '200 Settings applied successfully'}
                set_config.append(cam_data)

        return jsonify({'set_config': set_config})


def dictfrom_gpdata(data):
    res = dict()
    for key in data.keys():
        if isinstance(data[key], dict):
            res[key] = dict()
            res[key] = dictfrom_gpdata(data[key])
        if isinstance(data[key], gp.gphoto2.ConfigItem):
            res[key] = dict()
            res[key]['params'] = dict()
            res[key]['params']['value'] = data[key].value
            res[key]['params']['label'] = data[key].label
    return res


@app.route('/api/v0.1/config', methods=['GET'])
@cross_origin()
def get_config():
    cam_list = gp.list_cameras()
    cams_total = len(cam_list)
    config = []
    os.system('/usr/bin/gphoto2 --reset')

    if cams_total == 0:
        return make_response(jsonify({'error': '45 No cameras connected'
                             }))
    else:
        cam = 0
        for cam in range(cams_total):
            camera_object = gp.list_cameras()[cam]

            try:
                get_conf_result = camera_object._get_config()
                camsettings = dictfrom_gpdata(get_conf_result)
            except Exception:

                # print(gp.gp_widget_get_child_by_name(get_conf_result, 'cameramodel'))

                cam_data = {'id': cam + 1,
                            'name': camera_object.model_name,
                            'error': str(sys.exc_info()[1])}
                config.append(cam_data)
            else:
                cam_data = {'id': cam + 1,
                            'name': camera_object.model_name,
                            'settings': camsettings}
                config.append(cam_data)

        return jsonify({'config': config})


@app.route('/static/<path:path>')
def send_js(path):
    return send_from_directory('static', path)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found, unknown API request'
                         }), 404)

# if __name__ == '__main__':
#    app.run(debug=True, host='0.0.0.0')

if __name__ == '__main__':
    from waitress import serve
    serve(app, host='0.0.0.0', port=5000)
