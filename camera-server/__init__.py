"""
REST API service for DIgital Cameras control, special for MegaSecondShop
"""

__author__ = 'Maskin Victor'
__mainttainer__ = __authoe__

__email__ = 'hitmany@hitmany.net'
__license__ = 'MIT'
__version__ = '0.0.1'


__all__ = (
    '__author__',
    '__email__',
    '__license__',
    '__maintainer__',
    '__version__',
)
